//
//  AppDelegate.swift
//  HockeyMenuBarApp
//
//  Created by Taylor Guidon on 2/1/16.
//  Copyright © 2016 Taylor Guidon. All rights reserved.
//

import Cocoa
import Alamofire

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate, NSUserNotificationCenterDelegate {

@IBOutlet weak var statusMenu: NSMenu!
@IBOutlet weak var getPostsMenuItem: NSMenuItem!
@IBOutlet weak var soundEnabledMenuItem: NSMenuItem!

struct redditData {
    var title: String?
    var url: String?
}

var debug = false
var data = redditData(title: nil, url: nil)
let waitTime: NSTimeInterval = 60.0
let url = "https://www.reddit.com/r/hockey/new/.json"
  
let nhl = NHLTeams()
var timer = NSTimer()
let statusItem = NSStatusBar.systemStatusBar().statusItemWithLength(-1)
let defaults = NSUserDefaults.standardUserDefaults()
  
// MARK: - User Notification Center Delegate Methods

func userNotificationCenter(center: NSUserNotificationCenter,
    didActivateNotification notification: NSUserNotification) {
        if (notification == notification) {
            if let urlString = notification.userInfo?["url"] as? String,
                let url = NSURL(string: urlString) {
                // Open the url in the browser
                NSWorkspace.sharedWorkspace().openURL(url)
            }
        }
}

func userNotificationCenter(center: NSUserNotificationCenter,
      shouldPresentNotification notification: NSUserNotification) -> Bool {
        return true
}
  
// MARK: - Launch

func applicationDidFinishLaunching(aNotification: NSNotification) {
    NSUserNotificationCenter.defaultUserNotificationCenter().delegate = self
    // Check off menu item
    getPostsMenuItem.state = 1
    soundEnabledMenuItem.state = 1

    // turn on sound
    defaults.setBool(true, forKey: "soundEnabled")

    // Schedule and start the reddit polling
    scheduleTimer()
    timer.fire()

    // Menu bar icon setup
    let icon = NSImage(named: "MenuIcon")
    icon?.template = true
    statusItem.image = icon
    statusItem.menu = statusMenu
}

// MARK: - Menu bar button actions

@IBAction func togglePolling(sender: NSMenuItem) {
    if sender.state == NSOnState {
        // remove check box and kill polling
        sender.state = NSOffState
        timer.invalidate()
    } else {
        // engage polling
        sender.state = NSOnState
        scheduleTimer()
        timer.fire()
    }
}

@IBAction func toggleSound(sender: NSMenuItem) {
    if sender.state == NSOnState {
        sender.state = NSOffState
        defaults.setBool(false, forKey: "soundEnabled")
    } else {
        sender.state = NSOnState
        defaults.setBool(true, forKey: "soundEnabled")
    }
}
    
@IBAction func quit(sender: AnyObject) {
    NSApplication.sharedApplication().terminate(self)
}

@IBAction func openHockeyNew(sender: NSMenuItem) {
    let url = NSURL(string: "https://www.reddit.com/r/hockey/new/")
    // Open the url in the browser
    NSWorkspace.sharedWorkspace().openURL(url!)
}

@IBAction func openHockeySubreddit(sender: AnyObject) {
    let url = NSURL(string: "https://www.reddit.com/r/hockey")
    // Open the url in the browser
    NSWorkspace.sharedWorkspace().openURL(url!)
}

// MARK: - Get Hockey Data

func pollHockeyData() {
    getHockeyData(url)
}

func getHockeyData(url: String) {
    Alamofire.request(.GET, url)
    .responseJSON { response in
        if self.debug {
            print(response.response) // URL response
        }

        if let data = response.result.value {
        let json = JSON(data)

        if let title = json["data"]["children"][0]["data"]["title"].string,
            let url = json["data"]["children"][0]["data"]["url"].string {
                if url != self.data.url {
                    self.data.title = title
                    self.data.url = url
          
                    self.showNotification(title, url: url)
                }
            } else {
                print("Error: \(json["data"]["children"][0]["data"]["title"])")
                print("Error: \(json["data"]["children"][0]["data"]["url"])")
            }
        }
    }
}

func scheduleTimer() {
    timer = NSTimer.scheduledTimerWithTimeInterval(waitTime, target: self,
        selector: "pollHockeyData", userInfo: nil, repeats: true)
}
  
// MARK: - Notification settings

func showNotification(title: String, url: String) -> Void {
    let notification = NSUserNotification()
    
// FUTURE FEATURE IF REQUESTED
//
//    let teamNameFromTitle: String? = titleFilter(title)
//
//    if let team = teamNameFromTitle {
//        notification.title = "New \(team) Post in r/Hockey"
//    } else {
//        notification.title = "New Post in r/hockey"
//    }

    notification.title = "New Post in r/hockey 🏒"
    notification.informativeText = title
    notification.userInfo = ["url": url]

    let sound = defaults.boolForKey("soundEnabled")
    if sound {
        notification.soundName = NSUserNotificationDefaultSoundName
    } else {
        notification.soundName = nil
    }

    notification.presented
    notification.hasActionButton = true
  
    NSUserNotificationCenter.defaultUserNotificationCenter().deliverNotification(notification)
}

// FUTURE FEATURE IF REQUESTED
//
//func titleFilter(title: String) -> String? {  
//    for shortName in nhl.NHLTeamNames {
//        if title.rangeOfString(shortName) != nil {
//            return nhl.retriveTeamName(shortName)
//        }
//    }
//    return nil
//}

}
